import styled from 'styled-components'

export const Button = styled.button`
  border: none;
  box-shadow: 0 0 0 0;
  border-radius: 20px;
  color: #fff;
  height: 36px;
  outline: none;
  width: 100%;
`
