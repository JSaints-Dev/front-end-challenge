import styled from 'styled-components'

export const AreaTextField = styled.div`
  align-items: center;
  border-radius: 20px;
  border-color: #1bbadd;
  border-style: solid;
  border-width: thin;
  display: flex;
  height: 36px;
  justify-content: center;
  padding: 5px;
  width: 100%;

  svg {
    margin-right: 10px;
    color: #1bbadd;
  }
`

export const Input = styled.input`
  border: none;
  box-shadow: 0 0 0 0;
  color: grey;
  outline: none;
  width: 70%;
`

export const Select = styled.select`
  border: none;
  box-shadow: 0 0 0 0;
  background-color: #fff;
  color: gray;
  outline: none;
  width: 70%;

`
