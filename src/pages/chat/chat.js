import React from 'react'
import t from 'prop-types'
import io from 'socket.io-client'

function Chat ({ match }) {
  const baseUrl = 'http://localhost:3333'
  const chatroomId = match.params.id
  const socket = io(baseUrl, {
    query: {
      token: localStorage.getItem('CC_Token')
    }
  })
  return (
    <div>Chat</div>
  )
}

Chat.propTypes = {
  match: t.object
}

export default Chat
