import React, { useCallback, useContext, useState } from 'react'
import { Link } from 'react-router-dom'
import { AuthContext } from 'contexts/auth'
import styled from 'styled-components'
import { Button, Grid, Paper as MuiPaper } from '@material-ui/core'
import { Email, Lock } from '@material-ui/icons'
import { AreaTextField, Input } from 'ui/text-field'

import { REGISTER } from 'routes'

function Login () {
  const { login } = useContext(AuthContext)
  const [valueEmail, setValueEmail] = useState('')
  const [valuePassword, setValuePassword] = useState('')

  const handleEmail = useCallback(e => {
    const value = e.target.value
    setValueEmail(value)
  }, [])
  const handlePassword = useCallback(e => {
    const value = e.target.value
    setValuePassword(value)
  }, [])

  return (
    <ContainerLoginPage>
      <Paper>
        <Grid
          container
          spacing={4}
          justify='center'
          alignItems='center'
        >
          <Grid item xs={10}>
            <AreaTextField>
              <Email fontSize='small' />
              <Input
                type='text'
                onKeyUp={handleEmail}
                onChange={handleEmail}
                placeholder='Digite seu email'
              />
            </AreaTextField>
          </Grid>

          <Grid item xs={10}>
            <AreaTextField>
              <Lock fontSize='small' />
              <Input
                type='password'
                onKeyUp={handlePassword}
                onChange={handlePassword}
                placeholder='Digite seu CPF'
              />
            </AreaTextField>

          </Grid>

          <Grid item xs={10}>
            <Divider />
          </Grid>

          <Grid item xs={10}>
            <Button
              variant='contained'
              color='secondary'
              onClick={() => login(valueEmail, valuePassword)}
              fullWidth
            >login
            </Button>
          </Grid>
          <Grid item xs={10}>
            <Link to={REGISTER}>
              <Button
                variant='contained'
                color='secondary'
                fullWidth
              >
                faça seu cadastro
              </Button>
            </Link>
          </Grid>
        </Grid>
      </Paper>
    </ContainerLoginPage>
  )
}

const Divider = styled.div`
  background-color: #1bbadd;
  height: 1px;
  width: 100%;
`

const Paper = styled(MuiPaper).attrs({
  elevation: 3
})`
  height: 50vh;
  margin: auto;
  max-width: 474px;
  padding: 5vh 0px;
  width: 80%;

  button {
    background-color: #1bbadd;
  }
`

const ContainerLoginPage = styled.div`
  display:flex;
  height: 100vh;
`

export default Login
