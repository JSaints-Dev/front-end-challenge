import React, { useCallback, useContext, useState } from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import {
  Grid,
  Paper as MuiPaper
} from '@material-ui/core'
import { AreaTextField, Input, Select } from 'ui/text-field'
import { Button } from 'ui/button-call'
import {
  AccountBox,
  Email,
  Person,
  Phone,
  Room,
  QuestionAnswer
} from '@material-ui/icons'
import { AuthContext } from 'contexts/auth'
import { CHAT } from 'routes'

function Register () {
  const { register } = useContext(AuthContext)
  const [name, setName] = useState(null)
  const [cpf, setCpf] = useState(null)
  const [email, setEmail] = useState(null)
  const [phone, setPhone] = useState(null)
  const [state, setState] = useState(null)
  const [matters, setMatters] = useState('')

  // valor de optionsMatters e statesBr devem ser passado via API
  const optionsMatters = ['ELOGIO', 'RECLAMAÇÃO', 'SUGESTÃO', 'STATUS PEDIDO', 'CANCELAMENTO PEDIDO', 'ESQUECI A SENHA']
  const statesBr = ['BA', 'CE', 'MA', 'TO', 'PA']

  const handleInputName = (e) => {
    const value = e.target.value
    setName(value)
  }

  const handleInputCpf = (e) => {
    const value = e.target.value
    setCpf(value)
  }

  const handleInputEmail = (e) => {
    const value = e.target.value
    setEmail(value)
  }

  const handleInputPhone = (e) => {
    const value = e.target.value
    setPhone(value)
  }

  const handleInputState = (e) => {
    const value = e.target.value
    setState(value)
  }

  const handleInputMatters = useCallback((e) => {
    const value = e.target.value
    setMatters(value)
  }, [])

  const handleSubmit = useCallback(() => {
    register(
      name,
      cpf,
      email,
      phone,
      state,
      matters
    )
  }, [cpf, email, matters, name, phone, register, state])

  return (
    <ContainerRegisterPage>
      <Paper>
        <Grid
          container
          spacing={4}
          justify='center'
          alignItems='center'
        >
          <Grid item xs={10}>
            <Label>
              CADASTRE SUAS INFORMAÇÕES PARA INICIAR O ATENDIMENTO
            </Label>
          </Grid>

          <Grid item xs={10}>
            <AreaTextField>
              <Person fontSize='small' />
              <Input
                type='text'
                onKeyUp={handleInputName}
                onChange={handleInputName}
                placeholder='NOME'
              />
            </AreaTextField>
          </Grid>

          <Grid item xs={10}>
            <AreaTextField>
              <AccountBox fontSize='small' />
              <Input
                type='text'
                onKeyUp={handleInputCpf}
                onChange={handleInputCpf}
                placeholder='CPF'
              />
            </AreaTextField>
          </Grid>

          <Grid item xs={10}>
            <AreaTextField>
              <Email fontSize='small' />
              <Input
                type='text'
                onKeyUp={handleInputEmail}
                onChange={handleInputEmail}
                placeholder='EMAIL'
              />
            </AreaTextField>
          </Grid>

          <Grid item xs={10}>
            <AreaTextField>
              <Phone fontSize='small' />
              <Input
                type='text'
                onKeyUp={handleInputPhone}
                onChange={handleInputPhone}
                placeholder='TELEFONE'
              />
            </AreaTextField>
          </Grid>

          <Grid item xs={10}>
            <AreaTextField>
              <Room fontSize='small' />
              <Select
                defaultValue='ESTADO'
                onClick={handleInputState}
              >
                <option value='ESTADO' disabled hidden>ESTADO</option>
                {statesBr.map(el => (
                  <option key={el} value={el}>
                    {el}
                  </option>
                ))}
              </Select>
            </AreaTextField>
          </Grid>

          <Grid item xs={10}>
            <AreaTextField>
              <QuestionAnswer fontSize='small' />
              <Select
                defaultValue='ASSUNTO'
                onClick={handleInputMatters}
              >
                <option value='ASSUNTO' disabled hidden>ASSUNTO</option>
                {optionsMatters.map(el => (
                  <option key={el} value={el}>
                    {el}
                  </option>
                ))}
              </Select>

            </AreaTextField>

          </Grid>

          <Grid item xs={4}>
            <Link to={CHAT}>
              <Button
                onClick={handleSubmit}
              >
                INICIAR
              </Button>
            </Link>
          </Grid>

        </Grid>
      </Paper>
    </ContainerRegisterPage>
  )
}

const Label = styled.div`
  font-size: 1.2em;
  text-align: center;
  margin: auto;
  width: 80%;
`

const Paper = styled(MuiPaper).attrs({
  elevation: 3
})`
  height: 80vh;
  margin: auto;
  max-width: 474px;
  padding: 5vh 0px;
  width: 80%;

  button {
    background-color: #1bbadd;
  }
`

const ContainerRegisterPage = styled.div`
  display:flex;
  height: 100vh;
`

export default Register
