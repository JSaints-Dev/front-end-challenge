import React, { lazy, Suspense, useContext, useEffect } from 'react'
import t from 'prop-types'
import styled from 'styled-components'
import {
  Container as MuiContainer,
  LinearProgress
} from '@material-ui/core'
import { Switch, Route, Redirect } from 'react-router-dom'
import { AuthContext } from 'contexts/auth'

import { ATTENDANCE, LOGIN, CHAT, REGISTER } from 'routes'

const LoginPage = lazy(() => import('pages/login'))
const ChatPage = lazy(() => import('pages/chat'))
const AttendancePage = lazy(() => import('pages/attendance'))
const RegisterPage = lazy(() => import('pages/register'))

function App ({ location }) {
  const { employee, isLoggedIn } = useContext(AuthContext)

  if (isLoggedIn && employee && location.pathname === LOGIN) {
    return <Redirect to={ATTENDANCE} />
  }

  if (isLoggedIn && !employee && location.pathname === LOGIN) {
    return <Redirect to={CHAT} />
  }
  if (!isLoggedIn && location.pathname !== LOGIN && location.pathname !== REGISTER) {
    return <Redirect to={LOGIN} />
  }

  return (
    <Container>
      <Suspense fallback={<LinearProgress />}>
        <Switch>
          <Route path={LOGIN} component={LoginPage} />
          <Route path={CHAT} component={ChatPage} />
          <Route path={REGISTER} component={RegisterPage} />
          <Route component={AttendancePage} />
        </Switch>
      </Suspense>
    </Container>
  )
}

App.propTypes = {
  location: t.object
}

const Container = styled(MuiContainer)`
  min-width: 100%;
  min-height: 100vh;

  a {
    text-decoration: none;
  }
`

export default App
