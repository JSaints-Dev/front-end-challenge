import React, { createContext, useCallback, useState } from 'react'
import t from 'prop-types'
import axios from 'axios'
export const AuthContext = createContext()

function AuthProvider ({ children }) {
  const [employee, setEmployee] = useState(false)
  const [isLoggedIn, setIsLoggedIn] = useState(false)
  const [userInfo, setUserInfo] = useState(null)

  const baseUrl = 'http://localhost:3333'

  const register = useCallback((
    name,
    cpf,
    email,
    phone,
    state,
    matters
  ) => {
    const urlRegister = `${baseUrl}/user/register`
    const body = {
      name,
      cpf,
      email,
      phone,
      state,
      matters
    }

    axios({
      url: urlRegister,
      method: 'post',
      headers: { 'Content-Type': 'application/json' },
      data: body
    }).then((res) => {
      localStorage.setItem('CC_Token', res.data.data.token)
      setUserInfo(res.data.data.userInfo)
      setEmployee(res.data.data.userInfo.isEmployee)
      setIsLoggedIn(true)
    })
      .catch((err) => { console.log(err) })
  }, [])

  const login = useCallback((email, cpf) => {
    const urlLogin = `${baseUrl}/user/login`
    const body = { email, cpf }

    axios({
      url: urlLogin,
      method: 'post',
      headers: { 'Content-Type': 'application/json' },
      data: body
    }).then((res) => {
      localStorage.setItem('CC_Token', res.data.data.token)
      setUserInfo(res.data.data.userInfo)
      setEmployee(res.data.data.userInfo.isEmployee)
      setIsLoggedIn(true)
    })
      .catch((err) => console.log('[err]... ', err))
  }, [])

  const logout = useCallback(() => {
    console.log('[logout].....')
    setIsLoggedIn(false)
  }, [])

  return (
    <AuthContext.Provider value={{
      employee,
      isLoggedIn,
      register,
      login,
      logout,
      setEmployee,
      setIsLoggedIn
    }}
    >
      {children}
    </AuthContext.Provider>
  )
}

AuthProvider.propTypes = {
  children: t.node.isRequired
}

export default AuthProvider
